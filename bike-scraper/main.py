#!/bin/python

# python2 interpreter

import pybikes, json, time

def proc_station(st):

    dict = st.__dict__
    del dict['timestamp']
    return json.dumps(dict)

if __name__ == '__main__':

    to_bike = pybikes.get('to-bike')
    
    count = 0
    while 1:

        print('Up #' + str(count))
        count = count + 1
        to_bike.update()

        stations = map(proc_station, to_bike.stations)
        for i in range(3):
            print(stations[i])

        time.sleep(3)
